<?php

namespace Connectif\Routing\Filter;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Event\EventManager;
use Cake\Event\EventManagerTrait;
use Cake\ORM\TableRegistry;
use Cake\Routing\DispatcherFilter;
use Cake\Routing\Router;
use Connectif\ConnectifTags;
use User\Auth\Access;

class ConnectifTagsFilter extends DispatcherFilter
{

    public function beforeDispatch(Event $event)
    {
    }

    public function afterDispatch(Event $event)
    {
        $request = $event->getData('request');
        $response = $event->getData('response');

        if (strpos($response->type(), 'html') === false) {
            return;
        }

        if( $request->getParam( 'prefix') == 'admin') {
            return;
        }

        $this->_injectTags($request, $response);
    }

 
    protected function _injectTags($request, $response)
    {
        $tagsManager = new ConnectifTags($request, $response);
        $tagsManager->buildTags();
    }
}
