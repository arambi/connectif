<?php

namespace Connectif\Controller\Component;

use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Controller\Component;
use Cake\Event\EventDispatcherTrait;
use Cake\Controller\ComponentRegistry;

/**
 * ConnectifTags component
 */
class ConnectifTagsComponent extends Component
{
    use EventDispatcherTrait;

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public function shutdown(Event $event)
    {
        $this->setProduct();
        $this->setPurchase();

        $event = new Event('Connectif.customTags', $this);
        $this->getEventManager()->dispatch($event);
    }

    public function addCustomTag($name, $value)
    {
        $html = '<div class="'. $name .'" style="display:none">'. $value .'</div>';

        $current = Configure::read("ConnectifVars.customTags");

        if (!$current) {
            $current = '';
        }

        $current .= $html;
        Configure::write("ConnectifVars.customTags", $current);
    }

    public function setVar($key, $value)
    {
        Configure::write("ConnectifVars.$key", $value);
    }

    private function setProduct()
    {
        $request = $this->getController()->request;

        if (!($request->getParam('controller') == 'Products' && $request->getParam('action') == 'view')) {
            return;
        }

        $viewVars = $this->getController()->viewVars;

        if (isset($viewVars['content'])) {
            $this->setVar('product', $viewVars['content']);
        }
    }

    private function setPurchase()
    {
        $request = $this->getController()->request;

        if (!($request->getParam('controller') == 'Orders' && $request->getParam('action') == 'success')) {
            return;
        }

        $viewVars = $this->getController()->viewVars;

        if (isset($viewVars['content'])) {
            $this->setVar('purchase', $viewVars['content']);
        }
    }
}
