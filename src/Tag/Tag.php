<?php

namespace Connectif\Tag;

use Cake\Http\Response;
use Cake\Core\Configure;
use Cake\Http\ServerRequest;


class Tag
{
    protected $request;

    protected $response;

    protected $session;

    public function __construct(ServerRequest $request, Response $response)
    {
        $this->request = $request;
        $this->response = $response;
        $this->session = $request->getSession();
    }

    protected function getFromProductMethod($entity, $method)
    {
        if (method_exists($entity, $method)) {
            return $entity->$method();
        }
    }

    protected function getVar($key)
    {
        return Configure::read("ConnectifVars.$key");
    }

    public function render($name, $data, $extra = null)
    {
        $spans = [];

        foreach ($data as $key => $value)
        {
            if (!empty($value)) {
                $spans [] = '<span class="'. $key .'">'. $this->cleanValue($value) .'</span>';
            }
        }

        $content = implode("\n", $spans);

        if ($extra) {
            $content .= $extra;
        }

        return '<div class="'. $name .'" style="display: none">'. $content . '</div>';
    }

    private function cleanValue($value)
    {
        return str_replace("\n", " ", trim(strip_tags(html_entity_decode($value))));
    }

    protected function price($value)
    {
        return str_replace(',', '.', $value);
    }
}
