<?php

namespace Connectif\Tag;

use Store\Cart\Cart;
use Connectif\Tag\Tag;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Connectif\Tag\TagInterface;

class PurchaseTag extends Tag implements TagInterface
{

    private $order;

    public function build()
    {
        $order = $this->getVar('purchase');
        $this->order = $order;

        if (!$order) {
            return;
        }

        return $this->render('cn_purchase', [
            'cart_id' => $order->id,
            'purchase_id' => $order->id,
            'purchase_date' => $order->order_date->format('c'),
            'total_quantity' => $order->count_items,
            'total_price' => $this->price($order->total),
            'payment_method' => $order->payment_title,
        ], $this->getItems());
    }

    private function getItems()
    {
        $tags = [];

        foreach ($this->order->line_items as $item) {
            $data = [
                'quantity' => $item->quantity,
                'price' => $this->price($item->realPrice($item->subtotal, $item->tax_rate)),
                'url' => Router::url($item->product->link()),
                'product_id' => $item->product_id,
                'name' => $item->product->title,
                'image_url' => $item->photo,
                'unit_price' => $this->price($item->realPrice($item->price, $item->tax_rate)),
                'category' => $this->getFromProductMethod($item->product, 'connectifCategory'),
                'tag' => $this->getFromProductMethod($item->product, 'connectifTag'),
                'brand' => $this->getFromProductMethod($item->product, 'connectifBrand'),
                'rating_value' => $this->price($this->getFromProductMethod($item->product, 'connectifRating')),
                'thumbnail_url' => $this->getFromProductMethod($item->product, 'connectifThumbnail'),
                'unit_price_original' => $this->price($item->product->realPrice($item->product->original_price)),
                'unit_price_without_vat' => $this->price($item->product->price),
                'discounted_percentage' => $this->price($item->product->discount_price),
                'published_at' => $this->getFromProductMethod($item->product, 'connectifPublishedAt'),
            ];

            if ($item->photo) {
                $data['image_url'] = Router::url($item->photo, true);
            }

            $tags[] = $this->render('product_basket_item', $data);
        }

        return implode("\n", $tags);
    }

    
}
