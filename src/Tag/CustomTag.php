<?php 

namespace Connectif\Tag;

use Store\Cart\Cart;
use Connectif\Tag\Tag;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Connectif\Tag\TagInterface;

class CustomTag extends Tag implements TagInterface
{

    public function build()
    {
        return $this->getVar('customTags');
    }
}