<?php 

namespace Connectif\Tag;

interface TagInterface
{
    public function build();
}