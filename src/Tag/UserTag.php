<?php 

namespace Connectif\Tag;

use Connectif\Tag\Tag;
use Connectif\Tag\TagInterface;

class UserTag extends Tag implements TagInterface
{
    public function build()
    {
        $user = $this->session->read('Auth.User');

        if (!$user) {
            return;
        }

        return $this->render('cn_client_info', [
            'primary_key' => $user['email'],
            '_name' => $user['name'],
            '_surname' => !empty($user['lastname']) ? $user['lastname'] : null,
        ]);
    }
}