<?php 

namespace Connectif\Tag;

use Store\Cart\Cart;
use Connectif\Tag\Tag;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Connectif\Tag\TagInterface;

class ProductTag extends Tag implements TagInterface
{

    private $product;

    public function build()
    {
        $product = $this->getVar('product');

        if (!$product) {
            return;
        }

        return $this->render('cn_product_visited', [
            'url' => $product->link(),
            'name' => $product->title,
            'product_id' => $product->id,
            'description' => $this->getFromProductMethod($product, 'connectifDescription'),
            'image_url' => $this->getFromProductMethod($product, 'connectifImage'),
            'unit_price' => $this->price($product->realPrice($product->price, $product->tax)),
            'category' => $this->getFromProductMethod($product, 'connectifCategory'),
            'tag' => $this->getFromProductMethod($product, 'connectifTag'),
            'brand' => $this->getFromProductMethod($product, 'connectifBrand'),
            'rating_value' => $this->price($this->getFromProductMethod($product, 'connectifRating')),
            'thumbnail_url' => $this->getFromProductMethod($product, 'connectifThumbnail'),
            'unit_price_original' => $this->price($product->realPrice($product->original_price, $product->tax)),
            'unit_price_without_vat' => $this->price($product->price),
            'discounted_percentage' => $this->price($product->discount_price),
            'published_at' => $this->getFromProductMethod($product, 'connectifPublishedAt'),
        ]);
    }
}