<?php 

namespace Connectif\Tag;

use Store\Cart\Cart;
use Connectif\Tag\Tag;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Connectif\Tag\TagInterface;

class CartTag extends Tag implements TagInterface
{

    public function build()
    {
        $order = Cart::getOrder();

        if (!$order) {
            return;
        }


        return $this->render('cn_cart', [
            'cart_id' => $order->id,
            'total_quantity' => $order->count_items,
            'total_price' => $this->price($order->total),
        ], $this->getItems());
    }

    private function getItems()
    {
        $order = Cart::getOrder();

        $tags = [];

        foreach ($order->line_items as $item)
        {
            $data = [
                'quantity' => $item->quantity,
                'price' => $this->price($item->realPrice($item->subtotal, $item->tax_rate)),
                'url' => Router::url($item->product->link()),
                'product_id' => $item->product_id,
                'name' => $item->product->title,
                'image_url' => $item->photo,
                'unit_price' => $this->price($item->realPrice($item->price, $item->tax_rate))
            ];

            if ($item->photo) {
                $data ['image_url'] = Router::url($item->photo, true);
            }

            $tags [] = $this->render('product_basket_item', $data);
        }

        return implode("\n", $tags);
    }
}