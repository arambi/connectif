<?php

namespace Connectif;

use Cake\Http\Response;
use Connectif\Tag\CartTag;
use Connectif\Tag\UserTag;
use Cake\Http\ServerRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ConnectifTags
{
    private $request;

    private $response;

    private $session;

    public $tags = [];

    public function __construct(ServerRequest $request, Response $response)
    {
        $this->request = $request;
        $this->response = $response;
        $this->session = $request->getSession();
    }

    public function buildTags()
    {
        $this->userTag();
        $this->cartTag();
        $this->productTag();
        $this->purchaseTag();
        $this->customTag();

        $body = $this->response->body();

        $pos = strpos($body, '</body>');

        if ($pos === false) {
            return;
        }

        $body = substr($body, 0, $pos) . implode("\n", $this->tags) . substr($body, $pos);
        return $this->response->body($body);
    }

    public function addTag($html)
    {
        if ($html) {
            $this->tags[] = $html;
        }
    }

    private function getTagObject($name)
    {
        $class = "\Connectif\Tag\\$name";
        return new $class($this->request, $this->response);
    }

    public function userTag()
    {
        $tag = $this->getTagObject('UserTag');
        $this->addTag($tag->build());
    }

    public function cartTag()
    {
        $tag = $this->getTagObject('CartTag');
        $this->addTag($tag->build());
    }

    public function productTag()
    {
        $tag = $this->getTagObject('ProductTag');
        $this->addTag($tag->build());
    }

    public function purchaseTag()
    {
        $tag = $this->getTagObject('PurchaseTag');
        $this->addTag($tag->build());
    }

    public function customTag()
    {
        $tag = $this->getTagObject('CustomTag');
        $this->addTag($tag->build());
    }
}
