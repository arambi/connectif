<?php
namespace Consorcio\Test\TestCase\Tag;

use Cake\TestSuite\TestCase;
use Connectif\Tag\Tag;

/**
 * Consorcio\Model\Table\ProductsTable Test Case
 */
class TagTest extends TestCase
{
   public function testTag()
   {
       $tag = new Tag('cn_cart', [
           'cart_id' => '123',
           'total_quantity' => 12
       ]);

       $html = $tag->render();
       $this->assertTrue(strpos($html, '<span class="cart_id">123</span>') !== false);
   }
}
