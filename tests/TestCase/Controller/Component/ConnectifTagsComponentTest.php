<?php
namespace Connectif\Test\TestCase\Controller\Component;

use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use Connectif\Controller\Component\ConnectifTagsComponent;

/**
 * Connectif\Controller\Component\ConnectifTagsComponent Test Case
 */
class ConnectifTagsComponentTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Connectif\Controller\Component\ConnectifTagsComponent
     */
    public $ConnectifTags;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->ConnectifTags = new ConnectifTagsComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ConnectifTags);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
