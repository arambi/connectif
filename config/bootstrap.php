<?php

use Cake\Http\Server;
use Cake\Routing\DispatcherFactory;
use Connectif\Routing\Filter\ConnectifTagsFilter;

DispatcherFactory::add( new ConnectifTagsFilter(['priority' => 1]));
